CREATE DATABASE `news`;
USE `news`;

CREATE TABLE `news` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`headline` VARCHAR(255) NOT NULL,
    `content` TEXT,
    `datetime` DATETIME
 );   

CREATE TABLE `comments`(
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`news_id` INT UNSIGNED NOT NULL,
    `comments` TEXT
    );
    
    INSERT INTO `news` (`headline`, `content`)	
    VALUES ('Morning news', 'something');
    
    
    INSERT INTO `comments`(`news_id`, `comments`)
    VALUES (1, 'GOOD NEWS' );
